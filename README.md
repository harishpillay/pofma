# pofma

To get Singaporeans to propose edits and improvements to the "Protection from Online Falsehoods and Manipulation Bill". 

The bill was first read in the Singapore Parliament on April 1st 2019 (and yes, it was not a joke). The actual document is in a PDF form available at the Parliament of Singapore Website: https://www.parliament.gov.sg/docs/default-source/default-document-library/protection-from-online-falsehoods-and-manipulation-bill10-2019.pdf.

It will go through more readings in the parliament, possibly in May before it gets passed. Since the party in government has super majority, it is in all likelihood to pass. 

What this project is trying to do is to offer edits to the bill as presented to fix the bugs we collectively see as issues. This is probably the first time the citizenry has stepped up to fix a bill before it becomes the law of the land. Whethere it will be accepted or not, is anyone guess, but we must try.

Harish Pillay
26 April 2019