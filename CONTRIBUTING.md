How to contribute:

1) This is a collective effort and I am looking for help from those who have the experience in legal terminology and wordings to offer changes to the original text.
2) Please clone this repository and do your edits on the text and ensure that you are doing this under git version control
3) Send the PR (pull requests) and I will try to merge the requests as much as I can. 
4) I would love to have someone else help out in this as a committer and administrator as well.